package com.gitlab.druidgreeneyes.gate;

public class TestNotPassed extends RuntimeException {
    private TestNotPassed(String description) {
        super(description);
    }

    public static TestNotPassed of(String description) {
        return new TestNotPassed(description);
    }
}
