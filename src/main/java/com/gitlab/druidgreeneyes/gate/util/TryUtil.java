package com.gitlab.druidgreeneyes.gate.util;

import java.util.function.Function;

import io.vavr.control.Try;

public final class TryUtil {
    private TryUtil(){}

    public static <T, R> Function<Try<T>, R> fold(Function<Throwable, R> ifFailure, Function<T, R> ifSuccess) {
        return (Try<T> _try) -> _try.fold(ifFailure, ifSuccess);
    }
}
