package com.gitlab.druidgreeneyes.gate;

import io.vavr.collection.Seq;

public class GateNotPassed extends RuntimeException {
    private <T> GateNotPassed(T thing, Seq<Throwable> failures) {
        super(String.format("%s instance failed validation:\n\t\t%s",
            thing.getClass().getSimpleName(),
            String.join("\n\t\t", failures.map(Throwable::getMessage))));
    }

    public static <T> GateNotPassed of(T thing, Seq<Throwable> failures) {
        return new GateNotPassed(thing, failures);
    }
}
