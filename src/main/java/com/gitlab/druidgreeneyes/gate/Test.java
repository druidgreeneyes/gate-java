package com.gitlab.druidgreeneyes.gate;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import org.immutables.value.Value.Immutable;

import io.vavr.control.Try;

@Immutable
public interface Test<T> {
    String description();
    Predicate<T> predicate();

    public static <T> Test<T> of(String description, Predicate<T> predicate) {
        return _Test.of(description, predicate);
    }

    default public Test<T> unless(Test<T> other) {
        return of(
            String.format("%s UNLESS %s", description(), other.description()),
            other.predicate().negate().and(predicate())
        );
    }

    default public Test<T> when(Test<T> other) {
        return of(
            String.format("%s WHEN %s", description(), other.description()),
            other.predicate().and(predicate())
        );
    }
    
    default public Try<T> test(T thing) {
        return Try.of(() -> predicate().test(thing))
            .flatMap(s -> s
                ? Try.success(thing)
                : Try.failure(TestNotPassed.of(description())));
    }

    public static <T> UnaryOperator<Test<T>> unlessF(Test<T> qualifier) {
        return test -> test.unless(qualifier);
    }

    public static <T> UnaryOperator<Test<T>> whenF(Test<T> qualifier) {
        return test -> test.when(qualifier);
    }
    
    public static <T> Function<Test<T>, Try<T>> target(T thing) {
        return test -> test.test(thing);
    }
}
