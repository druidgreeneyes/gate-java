package com.gitlab.druidgreeneyes.gate;

import java.util.function.Function;
import java.util.function.Predicate;

import com.gitlab.druidgreeneyes.gate.util.TryUtil;

import org.immutables.value.Value.Immutable;

import io.vavr.collection.List;
import io.vavr.control.Try;

@Immutable
public interface Gate<T> {
    List<Test<T>> tests();

    public static <T> Gate<T> of(List<Test<T>> tests) {
        return _Gate.of(tests);
    }

    static <T> Gate<T> require(Test<T> test) {
        return of(List.of(test));
    }

    static <T> Gate<T> when(Test<T> qualifier, Gate<T> subGate) {
        return of(subGate.tests().map(Test.whenF(qualifier)));
    }

    static <T> Gate<T> unless(Test<T> qualifier, Gate<T> subGate) {
        return of(subGate.tests().map(Test.unlessF(qualifier)));
    }

    static <T> Gate<T> require(String description, Predicate<T> predicate) {
        return require(Test.of(description, predicate));
    }

    static <T> Gate<T> when(String description, Predicate<T> predicate, Gate<T> subGate) {
        return when(Test.of(description, predicate), subGate);
    }

    static <T> Gate<T> unless(String description, Predicate<T> predicate, Gate<T> subGate) {
        return unless(Test.of(description, predicate), subGate);
    }

    default public Gate<T> and(Test<T> test) {
        return of(tests().append(test));
    }

    default public Gate<T> and(String description, Predicate<T> predicate) {
        return and(Test.of(description, predicate));
    }

    default public Try<T> test(T thing) {
        return tests()
            .map(Test.target(thing)
                .andThen(TryUtil.fold(
                    x -> List.of(x),
                    __ -> List.<Throwable>empty())))
            .flatMap(x -> x)
            .transform(ts -> {
                if (ts.isEmpty())
                    return Try.success(thing);
                else
                    return Try.<T>failure(GateNotPassed.of(thing, ts));
            });
    }

    default public <R> R test(T thing, Function<T, R> ifSuccess, Function<Throwable, R> ifFailure) {
        return test(thing).fold(ifFailure, ifSuccess);
    }
}
