
@Value.Style(
    typeImmutable = "_*",
    allParameters = true, // all attributes will become constructor parameters
                          // as if they are annotated with @Value.Parameter
    visibility = Value.Style.ImplementationVisibility.PACKAGE, // Generated class will be always public
    defaults = @Value.Immutable(builder = false)) // Disable copy methods and builder
package com.gitlab.druidgreeneyes.gate;

import org.immutables.value.Value;
